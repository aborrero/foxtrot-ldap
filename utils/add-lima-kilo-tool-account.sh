#!/usr/bin/env bash
#
# Usage: add-lima-kilo-tool-account.sh NAME UID GID [DN_OF_MAINTAINER]

TOOL=${1:?TOOL required}
NEW_UID=${2:?UID required}
NEW_GID=${3:?GID required}
BASE_DN="dc=wmftest,dc=net"
TOOL_BASE_DN="ou=servicegroups,${BASE_DN}"
ADMIN_DN="cn=admin,${BASE_DN}"
ADMIN_PASS="admin"
MEMBER_DN=${4:-$ADMIN_DN}

/usr/bin/ldapadd -x -D "${ADMIN_DN}" -w "${ADMIN_PASS}" <<LDIF
dn: cn=toolsbeta.${TOOL},${TOOL_BASE_DN}
changetype: add
objectClass: groupOfNames
objectClass: posixGroup
objectClass: top
cn: toolsbeta.${TOOL}
gidNumber: ${NEW_GID}
member: ${MEMBER_DN}
LDIF
