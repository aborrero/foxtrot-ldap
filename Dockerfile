FROM docker.io/osixia/openldap:1.5.0

RUN apt-get -y update \
    && LC_ALL=C DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    bc

ADD bootstrap /container/service/slapd/assets/config/bootstrap
ADD environment /container/environment/01-custom
ADD utils /container/utils
